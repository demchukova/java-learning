package lab6;

import java.util.Scanner;

//7.12 (Duplicate Elimination) Use a one-dimensional array to solve the following problem:
//Write an application that inputs five numbers, each between 10 and 100, inclusive. As each number
//is read, display it only if it�s not a duplicate of a number already read. Provide for the �worst case,�
//in which all five numbers are different. Use the smallest possible array to solve this problem. Display
//the complete set of unique values input after the user enters each new value.
public class DuplicateElimination {
	public static void main(String args[]) {
		int[] array = new int[5];
		int temp, count = 0;
		Scanner input = new Scanner(System.in);
		for (int i = 0; i < 5; i++) {
			temp = input.nextInt();
			if (temp >= 10 && temp <= 100) {
				count = 0;
				for (int j = 0; j <= i; j++) {
					if (temp == array[j]) {
						count++;
						System.out.println("Input another value ");
					}

				}
				if (count == 0) {
					array[i] = temp;

				} else
					i--;

			} else {
				System.out.println("Print value beetwen 10 and 100");
				i--;
			}

		}
		for (int k : array)
			System.out.println(k);
		input.close();

	}
}
