package lab6;

//7.16 (Using the Enhanced for Statement) Write an application that uses an enhanced for state-
//ment to sum the double values passed by the command-line arguments. [Hint: Use the static
//method parseDouble of class Double to convert a String to a double value.]
public class Enbanced{
	public static void main(String args[]) {
		double sum = 0.0;
		for (String element : args) {
		    
			sum += Double.parseDouble(element);
		}

		System.out.println("The sum of elements: " + sum);
	
	}
}