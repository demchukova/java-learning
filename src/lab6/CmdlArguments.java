package lab6;

//(Command-Line Arguments) Rewrite Fig. 7.2 so that the size of the array is specified by the
//first command-line argument. If no command-line argument is supplied, use 10 as the default size
//of the array
public class CmdlArguments {

	// Initializing the elements of an array to default values of zero.

	public static void main(String[] args) {
		// declare variable array and initialize it with an array object

		int value;
		if (args.length == 0)
			value = 10;
		else
			value = Integer.parseInt(args[0]);

		int[] array = new int[value]; // create the array object
		System.out.printf("%s%8s%n", "Index", "Value"); // column headings
		// output each array element's value
		for (int counter = 0; counter < array.length; counter++)
			System.out.printf("%5d%8d%n", counter, array[counter]);

	} // end class InitArray

}