package lab6;

import java.util.Scanner;

//7.10 (Sales Commissions) Use a one-dimensional array to solve the following problem: A com-
//pany pays its salespeople on a commission basis. The salespeople receive $200 per week plus 9% of
//their gross sales for that week. For example, a salesperson who grosses $5,000 in sales in a week re-
//ceives $200 plus 9% of $5,000, or a total of $650. Write an application (using an array of counters)
//that determines how many of the salespeople earned salaries in each of the following ranges (assume
//that each salesperson’s salary is truncated to an integer amount):
//a) $200–299, b) $300–399, c) $400–499, d) $500–599 ,e) $600–699 ,f) $700–799, g) $800–899, h) $900–999
//i) $1,000 and over
//Summarize the results in tabular format.
public class SalesCommissions {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		int number, it, temp = 200, temp2 = 200, st = 0;
		it = input.nextInt();
		int[] table = new int[it];
		int[] counter = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

		for (int i = 0; i < it; i++) {

			number = input.nextInt();
			table[i] = (int) (200 + number * 0.09);
			System.out.println(table[i]);

			for (int j = 0; j < 9; j++) {
				temp2 += 100;
				if (table[i] >= temp && table[i] < temp2) {
					counter[j]++;
					st++;

				}
				if (st == 0 && j == 8)
					counter[j]++;
				temp += 100;

			}
			temp = 200;
			temp2 = 200;
		}
		System.out.println("Tut");
		temp = 200;
		temp2 = 200;
		for (int j = 0; j < 8; j++) {
			temp2 += 99;
			System.out.println("$" + temp + "-" + temp2 + " " + counter[j]);

			temp += 100;
			temp2++;
		}
		System.out.println("$1000 and over " + counter[8]);

		input.close();
	}
}