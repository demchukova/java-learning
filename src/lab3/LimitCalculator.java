package lab3;
public class LimitCalculator {
	private int accountNumber;
	private int balanceAtBeginning;
	private int totalItems;
	private int totalCredits;
	private int creditLimit;
	private int balanceAtEnd;

	public LimitCalculator(int accountNumber, int balanceAtBeginning,  int totalItems, int creditLimit, int totalCredits) {
		setAccountNumber(accountNumber);
		setBalanceAtBeginning(balanceAtBeginning);
		setTotalItems(totalItems);
		setCreditLimit(creditLimit);
		setTotalCredits(totalCredits);
		

	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public int getBalanceAtBeginning() {
		return balanceAtBeginning;
	}

	public void setBalanceAtBeginning(int balanceAtBeginning) {
		if (balanceAtBeginning >= 0)
			this.balanceAtBeginning = balanceAtBeginning;
	}

	public int getTotalItems() {
		return totalItems;
	}

	public void setTotalItems(int totalItems) {
		if (totalItems > 0)
			this.totalItems = totalItems;
	}

	public int getTotalCredits() {
		return totalCredits;
	}
	public void setCreditLimit(int creditLimit) {
		if (creditLimit < (this.balanceAtBeginning + this.totalItems))
		this.creditLimit = creditLimit;
	}

	public void setTotalCredits(int totalCredits) {
		if (totalCredits <= this.creditLimit)
			this.totalCredits = totalCredits;
		else
			System.out.println("Credit limit exceeded");
	}

	public int getCreditLimit() {
		return creditLimit;
	}

	


	public void setBalanceAtEnd() {
		this.balanceAtEnd = (this.balanceAtBeginning + this.totalItems - this.totalCredits);
	}
	public int getBalanceAtEnd() {
		return balanceAtEnd;
	}
}