package lab3;

//Gas Mileage) Drivers are concerned with the mileage their automobiles get. One driver has
//kept track of several trips by recording the miles driven and gallons used for each tankful. Develop
//a Java application that will input the miles driven and gallons used (both as integers) for each trip.
//The program should calculate and display the miles per gallon obtained for each trip and print the
//combined miles per gallon obtained for all trips up to this point. All averaging calculations should
//produce floating-point results. Use class Scanner and sentinel-controlled repetition to obtain the
//data from the user.
import java.util.Scanner;

public class GasMileageTest {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		int miles, galones, allmiles = 0, allgalones = 0;

		for (int i = 0; i < 5; i++) {
			System.out.println("write miles and galones");

			miles = input.nextInt();
			galones = input.nextInt();
			System.out.println("The trip result " + (float) miles / galones + " miles per 1 galone");
			allmiles += miles;
			allgalones += galones;

		}
		System.out.println("The average result " + (float) allmiles / allgalones + " miles per 1 galone");

		input.close();
	}
}
