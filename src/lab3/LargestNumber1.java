package lab3;

import java.util.Scanner;

//(Find the Two Largest Numbers) Using an approach similar to that for Exercise 4.21, find
//the two largest values of the 10 values entered. [Note: You may input each number only once.]

public class LargestNumber1 {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		int max, temp, max1;

		System.out.println("Write 10 numbers");
		max = input.nextInt();
		max1 = max;
		for (int i = 0; i < 9; i++) {
			temp = input.nextInt();
			if (max < temp) {
				max1 = max;
				max = temp;
			} else if (max > temp && max1 < temp)
				max1 = temp;

		}
		System.out.println("The first largest number is " + max +" the second is " + max1);
		input.close();
	}
}
