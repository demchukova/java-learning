package lab5;

//(Multiples) Write a method isMultiple that determines, for a pair of integers, whether the second integer is a multiple of the first. The method should take two integer arguments and return true if the second is a multiple of the first and false otherwise. [Hint: Use the remainder operator.]
//Incorporate this method into an application that inputs a series of pairs of integers (one pair at a time) and determines whether the second value in each pair is a multiple of the first
public class Multiples{
	public static boolean isMultiple(int first, int second)
	{
		if (second % first == 0)
			return true;
		return false;
	}
	public static void main(String args[])
	{
		System.out.println(isMultiple(10, 100));
		System.out.println(isMultiple(2, 11));
		System.out.println(isMultiple(3, 27));
	}
}