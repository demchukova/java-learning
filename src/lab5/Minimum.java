package lab5;
//(Find the Minimum) Write a method minimum3 that returns the smallest of three floating point
//numbers. Use the Math.min method to implement minimum3. Incorporate the method into an
//application that reads three values from the user, determines the smallest value and displays the result.
public class Minimum{

	
	public static double floatMin(double n1, double n2, double n3)
	{
		if (n1<Math.min(n2, n3))
			return n1 ; 
		return Math.min(n2, n3);
	}
	public static void main(String args[])
	{
		System.out.println(floatMin(2.0, 2.1, 2.0));
	}
}