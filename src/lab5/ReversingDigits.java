package lab5;



//(Reversing Digits) Write a method that takes an integer value and returns the number with
//its digits reversed. For example, given the number 7631, the method should return 1367. Incorporate
//the method into an application that reads a value from the user and displays the result

public class ReversingDigits {
	public static int reverse(int number) {
		int k = 0;
		while (number > 0) {
			k = k * 10 + number % 10;
			number = number / 10;

		}
		return k;

	}

	public static void main(String args[]) {
		System.out.println(reverse(1234));

	}
}