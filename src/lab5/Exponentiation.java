//(Exponentiation) Write a method integerPower(base, exponent) that returns the value of
//base exponent
//For example, integerPower(3, 4) calculates 34 (or 3 * 3 * 3 * 3). Assume that exponent is a positive,
//nonzero integer and that base is an integer. Use a for or while statement to control the calculation.
//Do not use any Math class methods. Incorporate this method into an application that reads
//integer values for base and exponent and performs the calculation with the integerPower method.
package lab5;

public class Exponentiation {
	public static int integerPower(int number, int power) {
		if (power == 0)
			return (1);
		else return number *integerPower(number, power-1);
	}
	public static void main(String args[]) {
	System.out.println(integerPower(10, 0));
	}
}