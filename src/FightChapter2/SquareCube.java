package FightChapter2;

//(Table of Squares and Cubes) Using only the programming techniques you learned in this
//chapter, write an application that calculates the squares and cubes of the numbers from 0 to 10 and
//prints the resulting values in table format, as shown below.

public class SquareCube {
	public static int square(int number) {
		return number * number;
	}

	public static int cube(int number) {
		return number * square(number);
	}

	public static void main(String args[]) {
		System.out.println("Number\tSquare\tCube");
		
		for (int i = 0; i <= 10; i++) {
			System.out.println(i + "\t"+square(i)+"\t"+cube(i));
		}

	}

}