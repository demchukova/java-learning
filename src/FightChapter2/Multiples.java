package FightChapter2;

import java.util.Scanner;

//Multiples) Write an application that reads two integers, determines whether the first is a
//multiple of the second and prints the result. [Hint: Use the remainder operator.]
public class Multiples {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		int number1, number2;
		number1 = input.nextInt();
		number2 = input.nextInt();
		
		if (number1 % number2 == 0)
			System.out.println("The first is multiple of second");
		else
			System.out.println("incorect numbers");
		input.close();
	}
}