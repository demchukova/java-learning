package FightChapter2;

import java.util.Scanner;

//2.32 (Negative, Positive and Zero Values) Write a program that inputs five numbers and determines
//and prints the number of negative numbers input, the number of positive numbers input and
//the number of zeros input.
public class NegativeAndPositive{
	public static void main(String args[])
	{
		Scanner inp = new Scanner(System.in);
		
		int number, positive = 0, negative = 0, zero = 0;
		for (int i = 0; i< 5; i++)
		{
			
		System.out.println("Input a number");
		number = inp.nextInt();
		if (number > 0)
			positive++;
		else if(number == 0)
			zero++;
		else
			negative++;
		}
	
		System.out.println("The positive numbers "+positive+". The negative "+negative+". The zeros "+zero);
		inp.close();
	}
}