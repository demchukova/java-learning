package FightChapter2;

import java.util.Scanner;

//(Odd or Even) Write an application that reads an integer and determines and prints whether
//it�s odd or even. [Hint: Use the remainder operator. An even number is a multiple of 2. Any multiple
//of 2 leaves a remainder of 0 when divided by 2.]
public class OddOrEven {

	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		int number;
		number = input.nextInt();
		if (number % 2 == 0)
			System.out.println("The number is even");
		else
			System.out.println("the number is odd");
		input.close();
	}
}