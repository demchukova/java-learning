package FightChapter2;

import java.util.Scanner;

//(Comparing Integers) Write an application that asks the user to enter two integers, obtains
//them from the user and displays the larger number followed by the words "is larger". If the numbers
//are equal, print the message "These numbers are equal". Use the techniques shown in Fig. 2.15.
public class Comparing {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		int num1, num2;
		System.out.println("Input two numbers");
		num1 = input.nextInt();
		num2 = input.nextInt();
		if (num1 < num2)
			System.out.println(num2 + " is larger");
		else if (num2 < num1)
			System.out.println(num1 + " is larger");
		else
			System.out.println("The numbers are equal");
	input.close();
	}
}