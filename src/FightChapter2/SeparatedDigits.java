package FightChapter2;

//(Separating the Digits in an Integer) Write an application that inputs one number consisting
//of five digits from the user, separates the number into its individual digits and prints the digits
//separated from one another by three spaces each. For example, if the user types in the number 42339,
//the program should print
public class SeparatedDigits {
	public static void separate(int number) {

		if (number >= 10000 && number <= 99999) {
			int first = number % 10;
			int second = (number - first) % 100 / 10;
			int third = (number - first - second) % 1000 / 100;
			int fourth = (number - first - second - third) % 10000 / 1000;
			int fifth = (number - first - second - third - fourth) % 100000 / 10000;
			System.out.println(fifth + " " + fourth + " " + third + " " + second + " " + first);
		} else System.out.println("Input the corect number");
	}

	public static void main(String args[]) {
		separate(12341);

	}
}