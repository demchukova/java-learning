package FightChapter2;

import java.util.Scanner;

//2.17 (Arithmetic, Smallest and Largest) Write an application that inputs three integers from the
//user and displays the sum, average, product, smallest and largest of the numbers. Use the techniques
//shown in Fig. 2.15. [Note: The calculation of the average in this exercise should result in an integer
//representation of the average. So, if the sum of the values is 7, the average should be 2, not
//2.3333....]
public class ArithmeticsExtended {
	public static int max(int number1, int number2) {
		if (number1 < number2) {
			return number2;
		}
		return number1;
	}

	public static int min(int number1, int number2) {
		if (number1 < number2) {
			return number1;
		}
		return number2;
	}

	public static void main(String[] args) {
		int number1, number2, number3, sum, product;
		double avg;
		Scanner inp = new Scanner(System.in);

		// input numbers
		System.out.println("Input your first value");
		number1 = inp.nextInt();
		System.out.println("input your second value");
		number2 = inp.nextInt();
		System.out.println("input your third value");
		number3 = inp.nextInt();

		// calculate sum of numbers
		sum = number1 + number2 + number3;

		// calculate average of numbers
		avg = (number1 + number2 + number3) / 3.0;

		// calculate product of numbers
		product = number1 * number2 * number3;

		// comparition between numbers

		if ((number1 == number2) && (number1 == number3)) {
			System.out.println("The numbers are equal");
		} else {
			System.out.println("the max value is " + max(number3, max(number1, number2)));
			System.out.println("the min value is " + min(number3, min(number1, number2)));
		}
		// print calculated values
		System.out.println("the sum of numbers = " + sum);
		System.out.println("the avg of numbers = " + avg);
		System.out.println("the product of numbers = " + product);
		inp.close();
	}
}