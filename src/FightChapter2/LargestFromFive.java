package FightChapter2;

import java.util.Scanner;

//(Largest and Smallest Integers) Write an application that reads five integers and determines
//and prints the largest and smallest integers in the group. Use only the programming techniques you
//learned in this chapter.
public class LargestFromFive{
	public static int max(int number1, int number2) {
		if (number1 < number2) {
			return number2;
		}
		return number1;
	}
	public static void main(String args[]) {
		int number1, number2, number3, number4, number5;
		int temp1, temp2;
		Scanner inp = new Scanner(System.in);

		// input numbers
		System.out.println("Input your first value");
		number1 = inp.nextInt();
		System.out.println("input your second value");
		number2 = inp.nextInt();
		System.out.println("input your third value");
		number3 = inp.nextInt();
		System.out.println("input your fourth value");
		number4 = inp.nextInt();
		System.out.println("input your fifth value");
		number5 = inp.nextInt();
		temp1 = max(number1, number2);
		temp2 = max(number3, number4);
		if(number5 > max(temp1, temp2))
			System.out.println(number5 + " is lagrest");
		else System.out.println(max(temp1, temp2) + " is largest");
		inp.close();
		
	}
}