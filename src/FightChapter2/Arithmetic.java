package FightChapter2;

import java.util.Scanner;

//(Arithmetic) Write an application that asks the user to enter two integers, obtains them
//from the user and prints their sum, product, difference and quotient (division). Use the techniques
//shown in Fig. 2.7.
public class Arithmetic {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		int num1, num2, product, sum, difference;
		double  devision;
		System.out.println("Input two numbers");
		num1 = input.nextInt();
		num2 = input.nextInt();
		sum = num1 + num2;
		product = num1 * num2;
		difference = num1 - num2;
		devision = (double) num1/num2;
		System.out.println("The sum of numbers is equal with " + sum);
		System.out.println("The difference of numbers is equal with " + difference);
		System.out.println("The product of numbers is equal with " + product);
		System.out.println("The devision of numbers is equal with " + devision);
		input.close();
	}
}