package lab4;

//(Calculating the Product of Odd Integers) Write an application that calculates the product
//of the odd integers from 1 to 15.

public class ProductOfOddNumbers{
	public static void main(String args[])
	{
		int product = 0;
		
		for (int i = 0; i <= 15; i++)
		{
			if( i %2 == 1) {
				System.out.println(" "+i);
				product += i;
			}
		}
		System.out.println("The product of odd numbers is equal with " + product);
	}
	
}