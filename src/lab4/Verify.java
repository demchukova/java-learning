package lab4;
//Assume that i = 1, j = 2, k = 3 and m = 2. What does each of the following statements print?
public class Verify {
	public static void main(String args[]) {
		int i = 1, j = 2, k = 3,m = 2;
		System.out.println("i = "+ i+", j = "+j+", k = "+k+", m = "+m);
		System.out.println("that's true that i == 1? "+ (i == 1));
		System.out.println("that's true that j == 3?"+ (j == 3));
		System.out.println("That's true that i >= 1 and j < 4? " + ((i >= 1) && (j < 4)));
		System.out.println("That's true that m <= 99 and k < m  "+((m <= 99) & (k < m)));
		System.out.println("That's true taht j >= i and k == m "+((j >= i) || (k == m)));
		System.out.println("That's true that (k + m < j) | (3 - j >= k) "+((k + m < j) | (3 - j >= k)));
		System.out.println("That's true that !(k > m) "+(!(k > m)));
	}
}