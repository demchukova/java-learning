package lab4;

import java.util.Scanner;

//(Find the Smallest Value) Write an application that finds the smallest of several integers.
//Assume that the first value read specifies the number of values to input from the user.

public class SmallestNumber {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		int number, min, temp;
		System.out.println("Enter number of numbers");
		number = input.nextInt();
		System.out.println("Enter integer number");
		min = input.nextInt();
		for (int i = 0; i < number; i++) {
			System.out.println("Enter integer number");
			temp = input.nextInt();
			if (temp < min) {
				min = temp;

			}

		}
		System.out.println("The smallest integer number is "+ min);
		input.close();

	}
}