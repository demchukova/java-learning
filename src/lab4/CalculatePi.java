package lab4;

//(Calculating the Value of π) Calculate the value of π from the infinite series
//Print a table that shows the value of π approximated by computing the first 200,000 terms of this
//series. How many terms do you have to use before you first get a value that begins with 3.14159?
public class CalculatePi {
	public static void main(String args[]) {
		int c = 0;
		float pi = 0;
		for (int i = 0; i <= 2000000; i++)
			if (i % 2 == 1) {
				c++;
				if (c % 2 == 1) {
					pi += (float) 4 / i;
				} else {
					pi -= (float) 4 / i;
				}
			}
		System.out.println(pi);
	}
}