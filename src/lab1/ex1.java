package lab1;


//
// Write an application that asks the user to enter two integers, obtains
// them from the user and displays the larger number followed by the words "is larger" . If the num-
// bers are equal, print the message "These numbers are equal" . Use the techniques shown in Fig. 2.15.
//
import java.util.Scanner;

public class ex1 {
	public static void main(String[] argc) {
		Scanner inp = new Scanner(System.in);
		int number1, number2;
		
		System.out.println("Input your first value");
		number1 = inp.nextInt();
		System.out.println("input your second value");
		number2 = inp.nextInt();

		if (number1 == number2) {
			System.out.println("the numbers are equal");
		} else if (number1 < number2) {
			System.out.println("the second value is largest " + number2);
		} else if (number1 > number2) {
			System.out.println("the first value is largest " + number1);
		}

	}
}