package lab1;

//(Checkerboard Pattern of Asterisks) Write an application that displays a checkerboard pat-
//tern, as follows:

//идея заключается в том, чтобы найти позицию элемента и определить четность и нечетность координат.
public class Checkerboard {
	public static void main(String[] args) {
		int i = 0, j = 0;

		while (i < 8) {
			j = 0;
			while (j < 16) {
				if ((i % 2 == 0) && (j % 2 == 0)) {
					System.out.print("*");
				} else if ((i % 2 == 0) && (j % 2 == 1)) {
					System.out.print(" ");
				} else if ((i % 2 == 1) && (j % 2 == 0)) {
					System.out.print(" ");
				} else if ((i % 2 == 1) && (j % 2 == 1)) {
					System.out.print("*");
				}

				j++;
			}
			i++;
			System.out.println();

		}
	}
}