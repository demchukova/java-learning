package lab1;

import java.util.Scanner;

//(World Population Growth Calculator) Use this web to determine the current world population and the annual world population growth rate. Write an application that inputs these values,
//then displays the estimated world population after one, two, three, four and five years

public class growthCalculator {
	public static void main(String[] args) {
		Scanner inp = new Scanner(System.in);
		long population = 0, currentPopulation, annualPopulation;
		int  i = 1;

		// input current and annual population
		System.out.println("Input current population");
		currentPopulation = inp.nextLong();
		System.out.println("Input annual population");
		annualPopulation = inp.nextLong();

		while (i <= 5) {
			population = currentPopulation + annualPopulation * i;
			System.out.printf("After %d year(s) population will be approximately equal with %d peoples \n", i, population);
			i++;
		}

	}
}